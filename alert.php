<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from theembazaar.com/demo/themesfolios/intellir-mobile/v1/alert.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 11:04:49 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Alert</title>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<!-- google font -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
		<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
		<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
		<link href="assets/css/style.css" rel="stylesheet">
		<link href="assets/css/responsive.css" rel="stylesheet">
	</head>
	<body><div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>
	<div class="wrapper">
			<!-- header -->
			<?php include("layout/header.php") ?>
			<!-- header_End -->

			<!-- Content_right -->
			<div class="container_full">

				<!-- sidebar -->
				<?php include("layout/sidebar.php") ?>
				<!-- sidebar_End -->

				<!--main contents start-->
				<main class="content_wrapper">
					<!--page title start-->
					<div class="page-heading">
						<div class="container-fluid">
							<div class="row d-flex align-items-center">
								<div class="col-12">
									<div class="page-breadcrumb">
										<h1>Alert Page</h1>
									</div>
								</div>
								<div class="col-12  d-md-flex">
									<div class="breadcrumb_nav">
										<ol class="breadcrumb">
											<li>
												<i class="fa fa-home"></i>
												<a class="parent-item" href="index-2.html">Home</a>
												<i class="fa fa-angle-right"></i>
											</li>
											<li class="active">
												Alert Page
											</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--page title end-->
					<div class="container-fluid">
						<div class="row">
							<div class=" col-12">
								<div class="card card-shadow mb-4">
									<div class="card-header">
										<div class="card-title">
											Default  Alerts
										</div>
									</div>
									<div class="card-body">
										<div class="alert alert-primary border-0" role="alert">
											This is a primary alert—check it out!
										</div>
										<div class="alert alert-secondary border-0" role="alert">
											This is a secondary alert—check it out!
										</div>
										<div class="alert alert-success border-0" role="alert">
											This is a success alert—check it out!
										</div>
										<div class="alert alert-danger border-0" role="alert">
											This is a danger alert—check it out!
										</div>
										<div class="alert alert-warning border-0" role="alert">
											This is a warning alert—check it out!
										</div>
										<div class="alert alert-info border-0" role="alert">
											This is a info alert—check it out!
										</div>
										<div class="alert alert-light border-0" role="alert">
											This is a light alert—check it out!
										</div>
										<div class="alert alert-dark border-0" role="alert">
											This is a dark alert—check it out!
										</div>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="card card-shadow mb-4">
									<div class="card-header">
										<div class="card-title">
											Dismissible Alerts
										</div>
									</div>
									<div class="card-body f12">
										<div class="alert alert-primary" role="alert">
											This is a primary alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-secondary" role="alert">
											This is a secondary alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-success" role="alert">
											This is a success alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-danger" role="alert">
											This is a danger alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-warning" role="alert">
											This is a warning alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-info" role="alert">
											This is a info alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-light" role="alert">
											This is a light alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-dark" role="alert">
											This is a dark alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class=" col-12">
								<div class="card card-shadow mb-4">
									<div class="card-header">
										<div class="card-title">
											Iconic  Alerts
										</div>
									</div>
									<div class="card-body f12">
										<div class="alert alert-primary border-0" role="alert">
											<i class="fa fa-bell-o pr-2"></i> This is a primary alert—check it out!
										</div>
										<div class="alert alert-secondary border-0" role="alert">
											<i class=" ti-alert pr-2"></i>This is a secondary alert—check it out!
										</div>
										<div class="alert alert-success border-0" role="alert">
											<i class=" ti-check-box pr-2"></i> This is a success alert—check it out!
										</div>
										<div class="alert alert-danger border-0" role="alert">
											<i class="  ti-na  pr-2"></i> This is a danger alert—check it out!
										</div>
										<div class="alert alert-warning border-0" role="alert">
											<i class="  ti-unlink pr-2"></i> This is a warning alert—check it out!
										</div>
										<div class="alert alert-info border-0" role="alert">
											<i class="  ti-light-bulb pr-2"></i> This is a info alert—check it out!
										</div>
										<div class="alert alert-light border-0" role="alert">
											<i class=" ti-check-box pr-2"></i> This is a light alert—check it out!
										</div>
										<div class="alert alert-dark border-0" role="alert">
											<i class=" ti-target pr-2"></i> This is a dark alert—check it out!
										</div>
									</div>
								</div>
							</div>
							<div class=" col-12">
								<div class="card card-shadow mb-4">
									<div class="card-header">
										<div class="card-title">
											Addional  Alerts
										</div>
									</div>
									<div class="card-body">
										<div class="alert alert-success" role="alert">
											<h4 class="alert-heading">Well done!</h4>
											<p>
												Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.
											</p>
											<hr>
											<p class="mb-0">
												Whenever you need to, be sure to use margin utilities to keep things nice and tidy.
											</p>
										</div>
										<div class="alert alert-warning" role="alert">
											<h4 class="alert-heading">Opps!</h4>
											<p>
												Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.
											</p>
											<hr>
											<p class="mb-0">
												Whenever you need to, be sure to use margin utilities to keep things nice and tidy.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class=" col-12">
								<div class="card card-shadow mb-4">
									<div class="card-header">
										<div class="card-title">
											Outline Alerts
										</div>
									</div>
									<div class="card-body f12">
										<div class="alert alert-outline alert-primary " role="alert">
											<i class="fa fa-bell-o pr-2"></i> This is a primary alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-outline alert-secondary " role="alert">
											<i class=" ti-alert pr-2"></i>This is a secondary alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-outline alert-success " role="alert">
											<i class=" ti-check-box pr-2"></i> This is a success alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-outline alert-danger " role="alert">
											<i class="  ti-na  pr-2"></i> This is a danger alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-outline alert-warning " role="alert">
											<i class="  ti-unlink pr-2"></i> This is a warning alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-outline alert-info " role="alert">
											<i class="  ti-light-bulb pr-2"></i> This is a info alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="alert alert-outline alert-dark " role="alert">
											<i class=" ti-target pr-2"></i> This is a dark alert—check it out!
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
								</div>
							</div>
							<div class=" col-12">
								<div class="card card-shadow mb-4">
									<div class="card-header">
										<div class="card-title">
											Outline Addional  Alerts
										</div>
									</div>
									<div class="card-body">
										<div class="alert alert-outline alert-success" role="alert">
											<h4 class="alert-heading">Well done!</h4>
											<p>
												Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.
											</p>
											<hr>
											<p class="mb-0">
												Whenever you need to, be sure to use margin utilities to keep things nice and tidy.
											</p>
										</div>
										<div class="alert alert-outline alert-warning" role="alert">
											<h4 class="alert-heading">Opps!</h4>
											<p>
												Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.
											</p>
											<hr>
											<p class="mb-0">
												Whenever you need to, be sure to use margin utilities to keep things nice and tidy.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</main>
				<!--main contents end-->
			</div>
			<!-- Content_right -->
			
			<!-- Footer -->
			<?php include("layout/footer.php") ?>
			<!-- Footer_End -->
		</div>
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/popper.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
		<script src="assets/js/custom.js" type="text/javascript"></script>
	</body>

<!-- Mirrored from theembazaar.com/demo/themesfolios/intellir-mobile/v1/alert.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 11:04:49 GMT -->
</html>
