<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hacaca App</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="assets/css/weather-icons.min.css" rel="stylesheet">
	<!--Morris Chart -->
	<link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
	<div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>

	<div class="wrapper">
		<?php include('layout/header.php') ?>

		<div class="container_full">

			<?php include('layout/sidebar.php') ?>

			<main class="content_wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<div class="panel profile-cover">
								<div class="profile-cover__img">
									<img src="assets/images/user1.png" alt="">
									<h3 class="h3">Artamas Mandiri, PT</h3>
								</div>
								<div class="profile-cover__action bg--img" data-overlay="0.3"></div>

								<div class="profile-cover__info">
									<div class="card">
										<div class="card-body">
											<ul class="nav nav-pills mb-4" role="tablist">
												<li class="nav-item">
													<a class="nav-link active" data-toggle="tab" href="#tab-ekspor">Ekspor</a>
												</li>

												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#tab-impor">Impor</a>
												</li>

												<li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#tab-domestik">Domestik</a>
												</li>

											</ul>

											<div class="tab-content">
												<div class="tab-pane active" id="tab-ekspor" role="tabpanel">
													Build responsive, mobile-first projects on the web with the world's most popular front-end component library. Bootstrap is
													an open source toolkit for developing with HTML, CSS, and JS. Quickly prototype your ideas or build your entire
													app with our Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful plugins
													built on jQuery.
												</div>
												<div class="tab-pane" id="tab-impor" role="tabpanel">
													Include Bootstrap's source Sass and JavaScript files via npm, Composer or Meteor. Package managed installs don't include
													documentation, but do include our build system and readme.
												</div>
												<div class="tab-pane" id="tab-domestik" role="tabpanel">
													Take Bootstrap 4 to the next level with official premium themes—toolkits built on Bootstrap with new components and plugins,
													docs, and build tools.
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- <div class="profile-cover__info">
									<ul class="nav">
										<li>
											<strong>26</strong>Hacaca
										</li>
										<li>
											<strong>33</strong>Indotrans
										</li>
										<li>
											<strong>136</strong>Total Order
										</li>
									</ul>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>
	</div>
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="assets/js/custom.js" type="text/javascript"></script>
</body>

</html>