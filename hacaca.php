<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hacaca App</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="assets/css/weather-icons.min.css" rel="stylesheet">
	<!--Morris Chart -->
	<link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
	<div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>

	<div class="wrapper">
		<?php include('layout/header.php') ?>

		<div class="container_full">

			<?php include('layout/sidebar.php') ?>

			<div class="content_wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">
							<a href="hc-ekspor.php">
								<div class="card text-white bg_green mb-4 mt-4">
									<div class="card-body d-flex">
										<div class="col-4">
											<img style="max-width: 80%" class="card-img" src="assets/images/ekspor.png" alt="Indotrans">
										</div>
										<div class="col-8">
											<h1 style="margin: 0px 0px 8px 0px" class="card-title text-light">Ekspor</h1>
											<span class="card-text">Ini deskripsi menu</span>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-12">
							<a href="hc-impor.php">
								<div class="card text-white bg-primary mb-4">
									<div class="card-body d-flex">
										<div class="col-4">
											<img style="max-width: 80%" class="card-img" src="assets/images/impor.png" alt="Indotrans">
										</div>
										<div class="col-8">
											<h1 style="margin: 0px 0px 8px 0px" class="card-title text-light">Impor</h1>
											<span class="card-text">Ini deskripsi menu</span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>