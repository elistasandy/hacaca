<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hacaca App</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="assets/css/weather-icons.min.css" rel="stylesheet">
	<!--Morris Chart -->
	<link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
	<div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>

	<div class="wrapper">
		<?php include('layout/header.php') ?>

		<div class="container_full">

			<?php include('layout/sidebar.php') ?>

			<div class="content_wrapper">
				<div class="container-fluid">
					<div class="row">
                        <div class="col-12">
                            <div class="card mb-3">
                                <div class="card-header bg-primary">
                                    <h2 class="card-title cl_lightgrey">Detail Completed Order</h2>					
                                </div>
                                <div class="card-body cl_darkgrey">
                                    <div class="row">
                                        <div class="col-4">
                                            <p class="card-text">Nomor OKA</p>
                                        </div>
                                        <div class="col-8 d-flex">
                                            <span class="card-text">: &nbsp;</span>
                                            <p class="card-text">IDN09304702</p>
                                        </div>
                                        <div class="col-4">
                                            <p class="card-text">Tujuan</p>
                                        </div>
                                        <div class="col-8 d-flex">
                                            <span class="card-text">: &nbsp;</span>
                                            <p class="card-text">Plurk Pte Ltd (Malaysia)</p>
                                        </div>
                                        <div class="col-4">
                                            <p class="card-text">Pengirim</p>
                                        </div>
                                        <div class="col-8 d-flex">
                                            <span class="card-text">: &nbsp;</span>
                                            <p class="card-text">PT. Jaya Makmur Gemilang (Indonesia)</p>
                                        </div>
                                        <div class="col-4">
                                            <p class="card-text">Status</p>
                                        </div>
                                        <div class="col-8 d-flex" style="vertical-align: middle">
                                            <span class="card-text">: &nbsp;</span>
                                            <div class="badge badge-pill badge-primary">Impor</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="baseline baseline-border">
                                        <div class="baseline-list baseline-border baseline-info">
                                            <div class="baseline-info">
                                                <div class="cl_info d-flex">
                                                    <div class="col-9" style="padding:0px 0px 0px 0px">Selasa, 3 Agustus 2019</div>
                                                    <div class="col-3 text-right" style="padding:0px 0px 0px 0px">13:10</div>
                                                </div>
                                                <span class="text-muted">Paket sampai di tujuan</span>
                                            </div>
                                        </div>
                                        <div class="baseline-list baseline-border baseline-info">
                                            <div class="baseline-info">
                                                <div class="cl_info d-flex">
                                                    <div class="col-9" style="padding:0px 0px 0px 0px">Senin, 2 Agustus 2019</div>
                                                    <div class="col-3 text-right" style="padding:0px 0px 0px 0px">08:00</div>
                                                </div>
                                                <span class="text-muted">Paket dalam pengantaran</span>
                                                <div class="row text-muted">
                                                    <div class="col-4">
                                                        <span>Nopol Truk</span>
                                                    </div>
                                                    <div class="col-8 d-flex">
                                                        <span class="card-text">: &nbsp;</span>
                                                        <span>L 8885 HC</span>
                                                    </div>
                                                    <div class="col-4">
                                                        <span>Supir</span>
                                                    </div>
                                                    <div class="col-8 d-flex">
                                                        <span class="card-text">: &nbsp;</span>
                                                        <span>Solikin</span>
                                                    </div>
                                                    <div class="col-4">
                                                        <span>Phone</span>
                                                    </div>
                                                    <div class="col-8 d-flex">
                                                        <span class="card-text">: &nbsp;</span>
                                                        <span>082287890988</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="baseline-list baseline-border baseline-info">
                                            <div class="baseline-info">
                                                <div class="cl_info d-flex">
                                                    <div class="col-9" style="padding:0px 0px 0px 0px">Minggu, 1 Agustus 2019</div>
                                                    <div class="col-3 text-right" style="padding:0px 0px 0px 0px">10:00</div>
                                                </div>
                                                <span class="text-muted">Paket loading truck</span>
                                            </div>
                                        </div>
                                        <div class="baseline-list baseline-border baseline-info">
                                            <div class="baseline-info">
                                                <div class="cl_info d-flex">
                                                    <div class="col-9" style="padding:0px 0px 0px 0px">Minggu, 1 Agustus 2019</div>
                                                    <div class="col-3 text-right" style="padding:0px 0px 0px 0px">08:00</div>
                                                </div>
                                                <span class="text-muted">Paket diregistrasi</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>