<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hacaca App</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="assets/css/weather-icons.min.css" rel="stylesheet">
	<!--Morris Chart -->
	<link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
	<div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>

	<div class="wrapper">
		<?php include('layout/header.php') ?>

		<div class="container_full">

			<?php include('layout/sidebar.php') ?>

			<div class="content_wrapper">
				<div class="container-fluid">
					<div class="row">
						<form class="col-12 mt-2">
							<div class="form-group">
								<input type="text" class="form-control form-control-lg" id="no_OKA" placeholder="Cari nomor OKA di sini">
							</div>
						</form>
						<div class="col-12">
							<a href="hc-imp-completed-detil.php">
								<div class="card mb-3">
									<div class="card-header bg-primary">
										<h2 class="card-title cl_lightgrey">No OKA: IDN09304702</h2>					
									</div>
									<div class="card-body cl_darkgrey">
										<div class="row">
											<div class="col-3">
												<p class="card-text">From</p>
											</div>
											<div class="col-9 d-flex">
												<span class="card-text">: &nbsp;</span>
												<p class="card-text">PT. Jaya Makmur Gemilang (Indonesia)</p>
											</div>
											<div class="col-3">
												<p class="card-text">To</p>
											</div>
											<div class="col-9 d-flex">
												<span class="card-text">: &nbsp;</span>
												<p class="card-text">Plurk Pte Ltd (Malaysia)</p>
											</div>
										</div>
									</div>
								</div>
							</a>
							<a href="hc-imp-completed-detil.php">
								<div class="card mb-3">
									<div class="card-header bg-primary">
										<h2 class="card-title cl_lightgrey">No OKA: SBY83794879</h2>					
									</div>
									<div class="card-body cl_darkgrey">
										<div class="row">
											<div class="col-3">
												<p class="card-text">From</p>
											</div>
											<div class="col-9 d-flex">
												<span class="card-text">: &nbsp;</span>
												<p class="card-text">PT. Jaya Makmur Gemilang (Indonesia)</p>
											</div>
											<div class="col-3">
												<p class="card-text">To</p>
											</div>
											<div class="col-9 d-flex">
												<span class="card-text">: &nbsp;</span>
												<p class="card-text">Plurk Pte Ltd (Malaysia)</p>
											</div>
										</div>
									</div>
								</div>
							</a>
							<a href="hc-imp-completed-detil.php">
								<div class="card mb-3">
									<div class="card-header bg-primary">
										<h2 class="card-title cl_lightgrey">No OKA: JKT23212343</h2>					
									</div>
									<div class="card-body cl_darkgrey">
										<div class="row">
											<div class="col-3">
												<p class="card-text">From</p>
											</div>
											<div class="col-9 d-flex">
												<span class="card-text">: &nbsp;</span>
												<p class="card-text">PT. Jaya Makmur Gemilang (Indonesia)</p>
											</div>
											<div class="col-3">
												<p class="card-text">To</p>
											</div>
											<div class="col-9 d-flex">
												<span class="card-text">: &nbsp;</span>
												<p class="card-text">Plurk Pte Ltd (Malaysia)</p>
											</div>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>