<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hacaca App</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="assets/css/weather-icons.min.css" rel="stylesheet">
	<!--Morris Chart -->
	<link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
	<div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>

	<div class="wrapper">
		<?php include('layout/header.php') ?>

		<div class="container_full">

			<?php include('layout/sidebar.php') ?>

			<div class="content_wrapper">
				<div class="container-fluid">
					<section class="chart_section">
						<div class="row">
							<form class="col-12 mt-2">
								<div class="form-group">
									<input type="text" class="form-control form-control-lg" id="no_OKA" placeholder="Masukan nomor OKA di sini">
								</div>
							</form>
							<div class="col-12">
								<a href="hacaca.php">
									<div class="card text-white bg-primary mb-4">
										<div class="card-body text-center">
											<img class="card-img img-fluid icon mb-3" src="assets/images/delivery.png" alt="Indotrans">
											<h1 class="card-title text-light">Hacaca</h1>
											<span>Some quick example text to build on the card title and make up the bulk of the.</span>
										</div>
									</div>
								</a>
							</div>
							<div class="col-12">
								<a href="indotrans.php">
									<div class="card text-white bg-info mb-4">
										<div class="card-body text-center">
											<img class="card-img img-fluid icon mb-3" src="assets/images/envelope.png" alt="Envelope">
											<h1 class="card-title text-light">Indotrans</h1>
											<span>Some quick example text to build on the card title and make up the bulk of the.</span>
										</div>
									</div>
								</a>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>

		<?php include('layout/footer.php') ?>
		<!-- Footer_End -->
	</div>
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

	<!--Morris Chart-->
	<script src="assets/js/index/morris-chart/morris.js"></script>
	<script src="assets/js/index/morris-chart/raphael-min.js"></script>
	<!--morris chart initialization-->
	<script src="assets/js/index/morris-chart/morris-init.js"></script>
	<!--chartjs Total Profit,New Orders,Yearly Revineue,New Users-->
	<script src="assets/js/Chart.min.js"></script>
	<script src="assets/js/chartjs-init.js"></script>

	<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>
