<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hacaca App</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="assets/css/weather-icons.min.css" rel="stylesheet">
	<!--Morris Chart -->
	<link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
	<div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>

	<div class="wrapper">
		<?php include('layout/header.php') ?>

		<div class="container_full">

            <?php include('layout/sidebar.php') ?>
            
            <main class="content_wrapper">
                <div class="container-fluid">
                    <div class="row">
						<div class=" col-12">
							<div class="card card-shadow mb-4">
								<div class="card-header">
									<div class="card-title cl_primary">
										List Member
									</div>
								</div>
								<div class="card-body">
                                    <a href="detil-member.php">
                                        <div class="media mb-4">
                                            <img class="align-self-center mr-3 rounded-circle" src="assets/images/user1.png" alt=" " style="width: 50px; height: 50px;">
                                            <div class="media-body">
                                                <p class="mb-0">
                                                    <strong class="">Artamas Mandiri, PT</strong>
                                                </p>
                                                <span>artamas</span>
                                            </div>
                                        </div>
                                    </a>
									<div class="media mb-4">
										<img class="align-self-center mr-3 rounded-circle" src="assets/images/user2.png" alt=" " style="width: 50px; height: 50px;">
										<div class="media-body">
											<p class="mb-0">
												<strong class="">Aman Sejahtera Perkasa, PT</strong>
											</p>
											<span>amansejahtera</span>
										</div>
									</div>
									<div class="media mb-4">
										<img class="align-self-center mr-3 rounded-circle" src="assets/images/user3.png" alt=" " style="width: 50px; height: 50px;">
										<div class="media-body">
											<p class="mb-0">
												<strong class="">Bima Arya Sejahtera, PT</strong>
											</p>
											<span>bimaarya</span>
										</div>
									</div>
									<div class="media mb-4">
										<img class="align-self-center mr-3 rounded-circle" src="assets/images/user3.png" alt=" " style="width: 50px; height: 50px;">
										<div class="media-body">
											<p class="mb-0">
												<strong class="">Budi Luhur, UD</strong>
											</p>
											<span>budiluhur</span>
										</div>
									</div>
									<div class="media mb-4">
										<img class="align-self-center mr-3 rounded-circle" src="assets/images/user2.png" alt=" " style="width: 50px; height: 50px;">
										<div class="media-body">
											<p class="mb-0">
												<strong class="">Boma Artama, PT</strong>
											</p>
											<span>bomaartama</span>
										</div>
									</div>
									<div class="media mb-4">
										<img class="align-self-center mr-3 rounded-circle" src="assets/images/user1.png" alt=" " style="width: 50px; height: 50px;">
										<div class="media-body">
											<p class="mb-0">
												<strong class="">Ganomeda, PT</strong>
											</p>
											<span>ganomeda</span>
										</div>
									</div>
									<div class="media mb-4">
										<img class="align-self-center mr-3 rounded-circle" src="assets/images/user3.png" alt=" " style="width: 50px; height: 50px;">
										<div class="media-body">
											<p class="mb-0">
												<strong class="">Integrasi Nusantara ,PT</strong>
											</p>
											<span>integrasi</span>
										</div>
									</div>
									<div class="media mb-4">
										<img class="align-self-center mr-3 rounded-circle" src="assets/images/user2.png" alt=" " style="width: 50px; height: 50px;">
										<div class="media-body">
											<p class="mb-0">
												<strong class="">Jaya Pramudia, PT</strong>
											</p>
											<span>jayapram</span>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>