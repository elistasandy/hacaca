<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Hacaca App</title>
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<!-- google font -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
	<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="assets/css/weather-icons.min.css" rel="stylesheet">
	<!--Morris Chart -->
	<link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
	<div id="loader_wrpper">
		<div class="loader_style"></div>
	</div>

	<div class="wrapper">
		<?php include('layout/header.php') ?>

		<div class="container_full">

            <?php include('layout/sidebar.php') ?>
            
            <main class="content_wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-shadow mb-4">
                                <div class="card-header">
                                    <div class="card-title cl_primary">
                                        Registrasi Member
                                    </div>
                                </div>
                                <div class="card-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="InputNamaPerusahaan">Nama Perusahaan</label>
                                            <div class="input-group">
                                                <span class="input-group-addon input-group-addon-bg-none">
                                                    <i class="icon-home cl_grey"></i>
                                                </span>
                                                <input type="text" class="form-control" id="InputNamaPerusahaan">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="InputUsername">Username</label>
                                            <div class="input-group">
                                                <span class="input-group-addon input-group-addon-bg-none">
                                                    <i class="icon-user cl_grey"></i>
                                                </span>
                                                <input type="text" class="form-control" id="InputUsername">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="InputNegara">Negara</label>
                                            <div class="input-group">
                                                <span class="input-group-addon input-group-addon-bg-none">
                                                    <i class="icon-user cl_grey"></i>
                                                </span>
                                                <input type="text" class="form-control" id="InputNegara">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="InputPassword">Password</label>
                                            <div class="input-group">
                                                <span class="input-group-addon input-group-addon-bg-none">
                                                    <i class="icon-lock cl_grey"></i>
                                                </span>
                                                <input type="password" class="form-control" id="InputPassword">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="InputRetypePassword">Re-type Password</label>
                                            <div class="input-group">
                                                <span class="input-group-addon input-group-addon-bg-none">
                                                    <i class="icon-key cl_grey"></i>
                                                </span>
                                                <input type="password" class="form-control" id="InputRetypePassword">
                                            </div>
                                        </div>
                                        <div class="align-items-right">
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="assets/js/custom.js" type="text/javascript"></script>
</body>
</html>