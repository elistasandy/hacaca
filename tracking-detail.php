<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hacaca App</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/ionicons.css" rel="stylesheet" type="text/css">
    <link href="assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="assets/css/weather-icons.min.css" rel="stylesheet">
    <!--Morris Chart -->
    <link rel="stylesheet" href="assets/js/index/morris-chart/morris.css">

    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>
    <div id="loader_wrpper">
        <div class="loader_style"></div>
    </div>

    <div class="wrapper">
        <?php include('layout/header.php') ?>

        <div class="container_full">

            <?php include('layout/sidebar.php') ?>

            <div class="content_wrapper" style="display: flex; align-items: flex-end;">
                <div class="map-wrapper">
                    <div id="map" class="map-content"></div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card mb-2">
                                <div class="card-body d-flex" style="padding: 10px">
                                    <div class="col-3" style="padding: 8px;">
                                        <img class="align-self-center" src="assets/images/box.png" alt=" ">
                                    </div>
                                    <div class="col-9">
                                        <div class="row">
                                            <div class="col-4">
                                                <p class="card-text">No OKA</p>
                                            </div>
                                            <div class="col-8 d-flex">
                                                <span class="card-text">: &nbsp;</span>
                                                <p class="card-text">IDN09304702</p>
                                            </div>
                                            <div class="col-4">
                                                <p class="card-text">Unit</p>
                                            </div>
                                            <div class="col-8 d-flex">
                                                <span class="card-text">: &nbsp;</span>
                                                <p class="card-text">SP3092372</p>
                                            </div>
                                            <div class="col-4">
                                                <p class="card-text">Tujuan</p>
                                            </div>
                                            <div class="col-8 d-flex">
                                                <span class="card-text">: &nbsp;</span>
                                                <p class="card-text">Jakarta</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-2">
                                <div class="card-body d-flex" style="padding: 10px">
                                    <div class="col-3" style="padding: 8px;">
                                        <img class="align-self-center" src="assets/images/delivery.png" alt=" ">
                                    </div>
                                    <div class="col-9">
                                        <div class="row">
                                            <div class="col-4">
                                                <p class="card-text">Nopol</p>
                                            </div>
                                            <div class="col-8 d-flex">
                                                <span class="card-text">: &nbsp;</span>
                                                <p class="card-text">L 5675 HC</p>
                                            </div>
                                            <div class="col-4">
                                                <p class="card-text">Driver</p>
                                            </div>
                                            <div class="col-8 d-flex">
                                                <span class="card-text">: &nbsp;</span>
                                                <p class="card-text">Solikin</p>
                                            </div>
                                            <div class="col-4">
                                                <p class="card-text">Phone</p>
                                            </div>
                                            <div class="col-8 d-flex">
                                                <span class="card-text">: &nbsp;</span>
                                                <p class="card-text">Solikin</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="assets/js/custom.js" type="text/javascript"></script>
    <script>
        // Initialize and add the map
        function initMap() {
            // The location of Uluru
            var uluru = {
                lat: -7.4662135,
                lng: 112.7004684
            };
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map'), {
                    zoom: 15,
                    center: uluru
                });
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&callback=initMap"></script>
</body>

</html>